package tronbase

const (
	CONTRACT_CLOSED ContractStatus = iota
	CONTRACT_OPENED
)
const (
	ORDER_WAITING OrderStatus = iota
	ORDER_PAID
	ORDER_CANCELLED
	ORDER_REFOUND
)

type ContractStatus int
type OrderStatus int

type ContractRsp struct {
	Code int             `json:"code"`
	Msg  string          `json:"msg"`
	Data []*ContractItem `json:"data"`
}

type ContractItem struct {
	ID              int64          `json:"id"`
	ContractType    string         `json:"contract_type"`
	ContractIcon    string         `json:"contract_icon"`
	ContractName    string         `json:"contract_name"`
	ContractAddr    string         `json:"contract_addr"`
	ContractDecimal int            `json:"contract_decimal"`
	Status          ContractStatus `json:"status"`
}

type OrderReq struct {
	CustomOrderId string  `json:"custom_order_id"`
	OrderTitle    string  `json:"order_title"`
	Contract      string  `json:"contract"`
	Amount        float64 `json:"amount"`
}
type OrderRsp struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data *Order `json:"data"`
}
type Order struct {
	ID         int64   `json:"id"`
	Token      string  `json:"token"`
	Address    string  `json:"address"`
	Contract   string  `json:"contract"`
	Amount     float64 `json:"amount"`
	CashierUrl string  `json:"cashier_url"`
}

type OrderInfo struct {
	ID               int64       `json:"id"`
	AppID            int64       `json:"app_id"`
	CustomOrderID    string      `json:"custom_order_id"`
	OrderTitle       string      `json:"order_title"`
	Contract         string      `json:"contract"`
	Amount           float64     `json:"amount"`
	Address          string      `json:"address"`
	Status           OrderStatus `json:"status"`
	PaidTime         int64       `json:"paid_time"`
	ValidPaymentTime int64       `json:"valid_payment_time"`
	CancelledTime    int64       `json:"cancelled_time"`
}

type Tx struct {
	ID          int64   `json:"id"`
	AppID       int64   `json:"app_id"`
	HitAddress  string  `json:"hit_address"`
	Block       int64   `json:"block"`
	TxHash      string  `json:"tx_hash"`
	FromAddress string  `json:"from_address"`
	ToAddress   string  `json:"to_address"`
	Contract    string  `json:"contract"`
	Amount      float64 `json:"amount"`
	CreatedOn   int64   `json:"created_on"`
}

type OrderDetail struct {
	Order *OrderInfo `json:"order"`
	Txs   []*Tx      `json:"txs"`
}

type OrderDetailRsp struct {
	Code int          `json:"code"`
	Msg  string       `json:"msg"`
	Data *OrderDetail `json:"data"`
}

type AddressInfo struct {
	ID            int64               `json:"id"`
	Address       string              `json:"address"`
	IsActivated   int                 `json:"is_activated"`
	TrxBalance    float64             `json:"trx_balance"`
	TokensBalance *map[string]float64 `json:"tokens_balance"`
	IsMain        int                 `json:"is_main"`
	Remark        string              `json:"remark"`
	CreatedOn     int64               `json:"created_on"`
}
type AddressRsp struct {
	Code int          `json:"code"`
	Msg  string       `json:"msg"`
	Data *AddressInfo `json:"data"`
}

type CollectionRsp struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

type WithdrawalRsp struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data struct {
		TxID string `json:"txid"`
	} `json:"data"`
}
