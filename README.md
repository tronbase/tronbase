# tronbase

TronBase Go SDK

## Getting started

安装
```
go get gitlab.com/tronbase/tronbase
```

初始化客户端
```go
const gateway = "https://api.tronbase.io" // TronBase网关地址，若私有化部署，此处为私有化服务地址
const appKey = "xxxxxx" // 钱包应用app key
const secretKey = "xxxxxx" // 钱包应用secret key

// 初始化客户端
client := tronbase.InitTronBase(gateway, appKey, secretKey)
```

### 1. 创建订单

[API文档](https://doc.tronbase.io/#/api/create_order)

```go
client.CreateOrder(&tronbase.OrderReq{
    CustomOrderId: "1093828372", // 业务自定义订单号
    OrderTitle:    "用户充值1000u", // 订单标题
    Contract:      "TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t", // 币种合约地址，可以调用开放币种接口获取
    Amount:        1000, // 订单金额
})
```

### 2. 查询订单

[API文档](https://doc.tronbase.io/#/api/query_order)

```go
client.QueryOrder(903528)
```


### 3. 查询开放币种

[API文档](https://doc.tronbase.io/#/api/open_tokens)

```go
client.GetContracts()
```

### 4. 创建地址

[API文档](https://doc.tronbase.io/#/api/create_address)

```go
client.CreateAddress("user8000")
```

### 5. 查询地址(余额)

[API文档](https://doc.tronbase.io/#/api/create_address)

```go
client.QueryAddress("TNrLwjzdxx7kGZk7VrwT8rAGLUPA1t8CGn", false)
```

### 6. 指定子地址归集

[API文档](https://doc.tronbase.io/#/api/single_collection)

```go
client.SingleCollection("TT4ZC1UPU88TeE4qXLBVcgS2xNAHKRowCR", "trx")
```

### 7. 批量归集

[API文档](https://doc.tronbase.io/#/api/batch_collection)

```go
client.BatchCollection("trx")
```

### 8. 发起提币

[API文档](https://doc.tronbase.io/#/api/withdrawal)

```go
client.Withdrawal("trx", "TPtKQtbg8HHU7XLBwKegLFGXSYJC3hJpqT", 1000)
```

> 具体参数类型可以直接查阅包定义。


### 回调验签

```go
// 获取系统签名
sysSign := tronbase.SignParams(params, secretKey)

// 签名对比
if strings.Compare(sign, sysSign) != 0 {
    // 签名不通过

    // TODO 你的逻辑...
    return
}


// 签名通过

// TODO 正常处理业务逻辑...

// 输出字符串 "success"

```