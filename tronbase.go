package tronbase

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"

	"github.com/go-resty/resty/v2"
)

type TronBase struct {
	Host      string
	AppKey    string
	SecretKey string
}

func (t *TronBase) request() *resty.Request {
	client := resty.New()
	client.DisableWarn = true

	return client.R()
}

// 创建订单
func (t *TronBase) CreateOrder(order *OrderReq) (*Order, error) {
	params := map[string]interface{}{
		"app_key":         t.AppKey,
		"custom_order_id": order.CustomOrderId,
		"order_title":     order.OrderTitle,
		"contract":        order.Contract,
		"amount":          order.Amount,
	}
	params["sign"] = SignParams(params, t.SecretKey)

	resp, err := t.request().SetHeader("Content-Type", "application/json").SetBody(params).Post(t.Host + "/gateway/order")
	if err != nil {
		return nil, err
	}

	rsp := &OrderRsp{}
	err = json.Unmarshal(resp.Body(), rsp)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode() == http.StatusOK {
		return rsp.Data, nil
	}

	if rsp.Msg != "" {
		return nil, errors.New(rsp.Msg)
	}
	return nil, errors.New("TronBase gateway is unavaliable")
}

// 查询订单
func (t *TronBase) QueryOrder(orderID int64) (*OrderDetail, error) {
	params := map[string]interface{}{
		"app_key":  t.AppKey,
		"order_id": fmt.Sprintf("%d", orderID),
	}

	params["sign"] = SignParams(params, t.SecretKey)

	data := url.Values{}
	for k, v := range params {
		data.Add(k, v.(string))
	}

	resp, err := t.request().SetQueryParamsFromValues(data).Get(t.Host + "/gateway/order")

	if err != nil {
		return nil, err
	}

	rsp := &OrderDetailRsp{}
	err = json.Unmarshal(resp.Body(), rsp)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode() == http.StatusOK {
		return rsp.Data, nil
	}

	if rsp.Msg != "" {
		return nil, errors.New(rsp.Msg)
	}
	return nil, errors.New("TronBase gateway is unavaliable")
}

// 获取合约地址
func (t *TronBase) GetContracts() ([]*ContractItem, error) {
	params := map[string]interface{}{
		"app_key": t.AppKey,
	}

	params["sign"] = SignParams(params, t.SecretKey)

	data := url.Values{}
	for k, v := range params {
		data.Add(k, v.(string))
	}

	resp, err := t.request().SetQueryParamsFromValues(data).Get(t.Host + "/gateway/active/contracts")

	if err != nil {
		return nil, err
	}

	rsp := &ContractRsp{}
	err = json.Unmarshal(resp.Body(), rsp)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode() == http.StatusOK {
		return rsp.Data, nil
	}

	if rsp.Msg != "" {
		return nil, errors.New(rsp.Msg)
	}
	return nil, errors.New("TronBase gateway is unavaliable")
}

// 创建地址
func (t *TronBase) CreateAddress(remark string) (*AddressInfo, error) {
	params := map[string]interface{}{
		"app_key": t.AppKey,
		"remark":  remark,
	}
	params["sign"] = SignParams(params, t.SecretKey)

	resp, err := t.request().SetHeader("Content-Type", "application/json").SetBody(params).Post(t.Host + "/gateway/address")
	if err != nil {
		return nil, err
	}

	rsp := &AddressRsp{}
	err = json.Unmarshal(resp.Body(), rsp)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode() == http.StatusOK {
		return rsp.Data, nil
	}

	if rsp.Msg != "" {
		return nil, errors.New(rsp.Msg)
	}
	return nil, errors.New("TronBase gateway is unavaliable")
}

// 查询余额
func (t *TronBase) QueryAddress(address string, active bool) (*AddressInfo, error) {
	params := map[string]interface{}{
		"app_key": t.AppKey,
		"address": address,
	}
	if active {
		params["active"] = "true"
	}

	params["sign"] = SignParams(params, t.SecretKey)

	data := url.Values{}
	for k, v := range params {
		data.Add(k, v.(string))
	}

	resp, err := t.request().SetQueryParamsFromValues(data).Get(t.Host + "/gateway/address")

	if err != nil {
		return nil, err
	}

	rsp := &AddressRsp{}
	err = json.Unmarshal(resp.Body(), rsp)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode() == http.StatusOK {
		return rsp.Data, nil
	}

	if rsp.Msg != "" {
		return nil, errors.New(rsp.Msg)
	}
	return nil, errors.New("TronBase gateway is unavaliable")
}

// 子地址归集
func (t *TronBase) SingleCollection(address, typeStr string) error {
	params := map[string]interface{}{
		"app_key": t.AppKey,
		"address": address,
		"type":    typeStr,
	}
	params["sign"] = SignParams(params, t.SecretKey)

	resp, err := t.request().SetHeader("Content-Type", "application/json").SetBody(params).Post(t.Host + "/gateway/balance/collection/single")
	if err != nil {
		return err
	}

	rsp := &CollectionRsp{}
	err = json.Unmarshal(resp.Body(), rsp)
	if err != nil {
		return err
	}
	if resp.StatusCode() == http.StatusOK {
		return nil
	}

	if rsp.Msg != "" {
		return errors.New(rsp.Msg)
	}

	return errors.New("TronBase gateway is unavaliable")
}

// 批量归集
func (t *TronBase) BatchCollection(typeStr string) error {
	params := map[string]interface{}{
		"app_key": t.AppKey,
		"type":    typeStr,
	}
	params["sign"] = SignParams(params, t.SecretKey)

	resp, err := t.request().SetHeader("Content-Type", "application/json").SetBody(params).Post(t.Host + "/gateway/balance/collection")
	if err != nil {
		return err
	}

	rsp := &CollectionRsp{}
	err = json.Unmarshal(resp.Body(), rsp)
	if err != nil {
		return err
	}
	if resp.StatusCode() == http.StatusOK {
		return nil
	}

	if rsp.Msg != "" {
		return errors.New(rsp.Msg)
	}

	return errors.New("TronBase gateway is unavaliable")
}

// 发起提币
func (t *TronBase) Withdrawal(contract, address string, amount float64) (string, error) {
	params := map[string]interface{}{
		"app_key":  t.AppKey,
		"contract": contract,
		"address":  address,
		"amount":   amount,
	}
	params["sign"] = SignParams(params, t.SecretKey)

	resp, err := t.request().SetHeader("Content-Type", "application/json").SetBody(params).Post(t.Host + "/gateway/withdrawal")
	if err != nil {
		return "", err
	}

	rsp := &WithdrawalRsp{}
	err = json.Unmarshal(resp.Body(), rsp)
	if err != nil {
		return "", err
	}
	if resp.StatusCode() == http.StatusOK {
		return rsp.Data.TxID, nil
	}

	if rsp.Msg != "" {
		return "", errors.New(rsp.Msg)
	}

	return "", errors.New("TronBase gateway is unavaliable")
}

func InitTronBase(host, appKey, secretKey string) *TronBase {
	return &TronBase{
		Host:      host,
		AppKey:    appKey,
		SecretKey: secretKey,
	}
}
