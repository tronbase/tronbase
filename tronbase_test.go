package tronbase

import (
	"fmt"
	"testing"
	"time"
)

const gateway = "https://api.tronbase.io"
const testAppKey = "AKRpUUNCNgVco28mohnvhj4U9L7TkTvkWNG6smNlJRVX21slTZoqA4Ji5xtMmYeu"
const testSecretKey = "SKORG6RMbhGu3iVm0m0CM1LvYwgVYQ4UVbYDLytqFDu822sSYwNEvBedZ1ZWMipQ"

func TestCreateOrder(t *testing.T) {
	client := InitTronBase(gateway, testAppKey, testSecretKey)

	order, err := client.CreateOrder(&OrderReq{
		CustomOrderId: fmt.Sprintf("%d", time.Now().Unix()),
		OrderTitle:    "用户充值1000u",
		Contract:      "TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t",
		Amount:        1000,
	})
	if err != nil {
		t.Errorf("create order err: %v", err)
	} else {
		if order.Amount != 1000 {
			t.Error("error order amount")
		}
		if order.Contract != "TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t" {
			t.Error("error order contract")
		}
	}
}

func TestQueryOrder(t *testing.T) {
	client := InitTronBase(gateway, testAppKey, testSecretKey)

	_, err := client.QueryOrder(903528)
	if err != nil {
		t.Errorf("query order err: %v", err)
	}
}

func TestContracts(t *testing.T) {
	client := InitTronBase(gateway, testAppKey, testSecretKey)

	_, err := client.GetContracts()
	if err != nil {
		t.Errorf("get contracts err: %v", err)
	}
}

func TestCreateAddress(t *testing.T) {
	client := InitTronBase(gateway, testAppKey, testSecretKey)

	_, err := client.CreateAddress("user8000")
	if err != nil {
		t.Errorf("create address err: %v", err)
	}
}

func TestQueryAddress(t *testing.T) {
	client := InitTronBase(gateway, testAppKey, testSecretKey)

	_, err := client.QueryAddress("TNrLwjzdxx7kGZk7VrwT8rAGLUPA1t8CGn", false)
	if err != nil {
		t.Errorf("query address err: %v", err)
	}
}

func TestSingleCollection(t *testing.T) {
	client := InitTronBase(gateway, testAppKey, testSecretKey)

	err := client.SingleCollection("TT4ZC1UPU88TeE4qXLBVcgS2xNAHKRowCR", "trx")
	if err != nil {
		t.Errorf("single collection err: %v", err)
	}
}

func TestBatchCollection(t *testing.T) {
	client := InitTronBase(gateway, testAppKey, testSecretKey)

	err := client.BatchCollection("trx")
	if err != nil {
		t.Errorf("batch collection err: %v", err)
	}
}

func TestWithdrawal(t *testing.T) {
	client := InitTronBase(gateway, testAppKey, testSecretKey)

	_, err := client.Withdrawal("trx", "TPtKQtbg8HHU7XLBwKegLFGXSYJC3hJpqT", 1000)
	if err.Error() != "可用余额不足" {
		t.Errorf("withdrawal err: %v", err)
	}
}
